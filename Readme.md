
# wsdm-share

Simple react component displaying share url and embed code (iframe) based
on provided serialised state.

Also provides a helper method to read the state from the url.

Can accept `uiLabels` object as a prop to override defaults or localise the component:

```
{
  share: "",
  embed: "",
  share_help_text: "",
  embed_help_text: "",
}
```

Run development server with:

```
$ npm i
$ make start
```

Build with:

```
$ make build
```

Default styling is provided with Tachyons classes. All elements have also
`wsd-share-` prefixed classes if you want to override defaults it should be
easy enough to do.

[Lapidus Interactive](http://lapidus.se)
