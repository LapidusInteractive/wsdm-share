import React, { Component } from "react"
import PropTypes from "prop-types"
import ReactDOM from "react-dom"

import Share, { stateFromUrl } from "../index"

// Dummy chart that will use the share component
class Chart extends Component {
  static propTypes = {
    indicator: PropTypes.string,
    coloredBy: PropTypes.string,
    filteredBy: PropTypes.object,
    splitBy: PropTypes.string,
    iconType: PropTypes.string,
    selected: PropTypes.array,
    opacityNonSelected: PropTypes.number,
    scaleType: PropTypes.string,
    embedded: PropTypes.bool,
    location: PropTypes.object,
  }

  static defaultProps = {
    coloredBy: "",
    filteredBy: {},
    splitBy: "",
    iconType: "flag",
    selected: [],
    opacityNonSelected: 0.15,
    scaleType: "",
    noConfig: false,
    embedded: false,
    location: typeof window !== "undefined" ? window.location : undefined,
  }

  constructor(props) {
    super(props)

    const initialState = {
      indicator: props.indicator || "",
      coloredBy: props.coloredBy,
      filteredBy: props.filteredBy,
      splitBy: props.splitBy,
      iconType: props.iconType,
      scaleType: props.scaleType,
      embedded: props.embedded,
      shareState: "",
      embedState: "",
      selected: props.selected,
      opacityNonSelected: props.opacityNonSelected,
      location: props.location,
    }

    this.state = this.props.location
      ? Object.assign(initialState, stateFromUrl(this.props.location))
      : initialState

    this.handleShareButtonClick = this.handleShareButtonClick.bind(this)
  }

  /**
   * Serialize the config needed for the chart to be recreated
   * from the url or clear that serialised state if true is passed
   * as the clear param
   * passed to ConfigUI
   * @param  {Bool} clear Whether to create or clear the share state
   * @return {Void}
   */
  handleShareButtonClick(clear = false) {
    if (clear)
      return this.setState({
        shareState: "",
        embedState: "",
      })

    const share = {
      indicator: this.state.indicator,
      year: this.state.year,
      splitBy: this.state.splitBy,
      coloredBy: this.state.coloredBy,
      filteredBy: this.state.filteredBy,
      selected: this.state.selected,
      iconType: this.state.iconType,
      opacityNonSelected: this.state.opacityNonSelected,
    }

    const embed = Object.assign({ embedded: true, noConfig: true }, share)

    this.setState({
      shareState: JSON.stringify(share),
      embedState: JSON.stringify(embed),
    })
  }

  render() {
    return (
      <div className="pt4-ns center mw8">
        <Share
          shareState={this.state.shareState}
          embedState={this.state.embedState}
          onShareButtonClick={this.handleShareButtonClick}
          location={this.state.location}
          iframeAttrs={{
            id: "chart_example",
            name: "chart_example",
            title: "Example Chart from example-chart.com",
          }}
        />
      </div>
    )
  }
}

ReactDOM.render(<Chart />, document.querySelector("#chart"))
