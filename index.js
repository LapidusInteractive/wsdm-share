import React from "react"
import PropTypes from "prop-types"

// helper for restoring state object from the url
export const stateFromUrl = (location, prop = "state") => {
  const { search } = location
  const searchArray = search.substring(1).split("=")
  const stateIndex = searchArray.indexOf(prop)
  return stateIndex > -1 && searchArray[stateIndex + 1]
    ? JSON.parse(decodeURIComponent(searchArray[stateIndex + 1]))
    : {}
}

const Share = ({
  url,
  shareState,
  embedState,
  onShareButtonClick,
  uiLabels = {},
  iframeAttrs,
  location: { protocol, host, pathname },
}) => {
  const getUrl = state =>
    url
      ? `${url}?state=${encodeURIComponent(state)}`
      : `${protocol}//${host}${pathname}?state=${encodeURIComponent(state)}`

  const iframeAttrsString = () => {
    const defaults = {
      id: "embeded_chart_iframe",
      name: "embeded_chart_iframe",
      title: "Embeded chart",
      width: "100%",
      height: "750",
      frameborder: "0",
      marginheight: "0",
      marginwidth: "0",
    }
    iframeAttrs = Object.assign(defaults, iframeAttrs, {
      src: getUrl(embedState),
    })
    let iframeAttrsString = ""
    for (const prop in iframeAttrs) {
      if (iframeAttrs.hasOwnProperty(prop))
        iframeAttrsString += ` ${prop}=${iframeAttrs[prop]}`
    }
    return iframeAttrsString
  }
  const iframeString = embedState
    ? `<iframe ${iframeAttrsString()}></iframe>`
    : ""

  const onEmbedClick = event => {
    event.preventDefault()
    onShareButtonClick(!!shareState)
  }

  const onInputClick = event => {
    event.target.select()
  }

  return (
    <div className="wsdm-share-wrapper ph1 ph5-ns tr">
      <a
        href=""
        className="wsdm-share-toggle dib mb2 link pa1 ba black br2 b--light-silver"
        onClick={onEmbedClick}
      >
        {`${uiLabels.share || "Share"}`}
      </a>
      {shareState && (
        <div className={"wsdm-share-box cf bg-near-white ph4 pb4 tl"}>
          <div
            className={`wsdm-share-area ${embedState ? "fl-ns w-50-ns" : ""}`}
          >
            <p className="wsdm-share-label f3 b mb1">
              {uiLabels.share || "Share"}
            </p>
            <p className="wsdm-share-help-text h3-ns w-90">
              {uiLabels.share_help_text ||
                `This url can be used to open this page with the chart
                in its current state. Use it to share with others or just
                as a bookmark for yourself.`}
            </p>
            <input
              className="wsdm-share-input w-90 db ba b--gray pa2 pa3-ns"
              onClick={onInputClick}
              defaultValue={getUrl(shareState)}
            />
          </div>
          {embedState && (
            <div className="wsdm-share-area fl-ns w-50-ns">
              <p className="wsdm-share-label f3 b mb1">
                {uiLabels.embed || "Embed"}
              </p>
              <p className="wsdm-share-help-text h3-ns w-90">
                {uiLabels.embed_help_text ||
                  `Copy this code and place it on your website
                where you'd like the chart to appear.`}
              </p>
              <input
                className="wsdm-share-input w-90 db ba b--gray pa2 pa3-ns"
                onClick={onInputClick}
                defaultValue={iframeString}
              />
            </div>
          )}
        </div>
      )}
    </div>
  )
}

Share.propTypes = {
  shareState: PropTypes.string.isRequired,
  onShareButtonClick: PropTypes.func.isRequired,
  embedState: PropTypes.string,
  uiLabels: PropTypes.object,
  location: PropTypes.object,
  url: PropTypes.string,
  iframeAttrs: PropTypes.object,
}

export default Share
